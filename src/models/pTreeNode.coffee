
ascc = window.ascc

class ascc.pTreeNode extends Backbone.RelationalModel
  relations:
    [
      type: 'HasMany'
      key: 'children'
      relatedModel: 'ascc.pTreeNode'
      reverseRelation:
        key: 'parent'
    ]

  defaults:
    parent: null
    orderIndex: -1
    label: ''

  initialize: ({@getSizeCallback, @onLayoutCallback}) ->
    @_getSize()
    @sizeInPixels = 0
    @listenTo( this, 'add:children', @handleAddChild )
    @listenTo( this, 'change:children', -> console.log arguments )

  setRelativeSize: ->
    @_getSize()
    @childrenSize = 0
    _.each @get( 'children' ).models, (child) => @childrenSize += child.setRelativeSize()
    @size + @childrenSize

  _getSize: ->
    @size = @getSizeCallback?(this, this.get( 'label' )) or 0

  reparent: ( newParent, newIndex ) ->
    console.log arguments
    @get( 'parent' ).get( 'children' ).remove( this )
    @set( 'parent', newParent )
    @set( 'orderIndex', newIndex )
    newParent.get( 'children' ).add( this )

  setSizeInPixels: (sizeLeft) ->
    @sizeInPixels = @onLayoutCallback?(this, @size, @childrenSize, sizeLeft ) or 0
    _.each @get( 'children' ).models, (child) => child.setSizeInPixels(sizeLeft - @sizeInPixels)

  handleAddChild: (addedModel, collection) ->
    console.log addedModel
    if addedModel.get( 'orderIndex' ) is -1
      addedModel.set( 'orderIndex', collection.length + 1 )
    @reorderChildren()

  reorderChildren: ->
    _.each @get( 'children' ).models, (child,index) => child.set( 'orderIndex',index )

class ascc.pTreeCollection extends Backbone.Collection
  model: ascc.pTreeNode
  comparator: 'orderIndex'

