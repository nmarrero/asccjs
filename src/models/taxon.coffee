ascc = window.ascc

class ascc.Taxon extends Backbone.Model
  defaults:
    name: ''
    minOKR: 0.0
    maxOKR: 0.0

  initialize: ( {name} )->
    ascc.Taxon._taxaNames[name] = this

  destroy: ->
    delete ascc.Taxon._taxaNames[@.get( 'name' )]

  name: ->
    newName = arguments[0]
    if newName != 'undefined'
      throw "Taxon #{val} already exists, #{@get( 'name' )} cannot be renamed" if ascc.Taxon._taxaNames[newName]
      @set( 'name', newName )
    @get( 'name' )

  minOKR: ->
    newOKR = arguments[0]
    if newOKR != 'undefined'
      @set( 'minOKR', newOKR )
    @get( 'minOKR' )

  maxOKR: ->
    newOKR = arguments[0]
    if newOKR != 'undefined'
      @set( 'maxOKR', newOKR )
    @get( 'maxOKR' )

ascc.Taxon._taxaNames = {}
ascc.Taxon.findByName = (name) ->
  ascc.Taxon._taxaNames?[name] or null
