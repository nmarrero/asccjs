loadjsfile = (filename) ->
  fileref = $("<script type='text/javascript' src='#{filename}' />")
  $("head").append(fileref)

loadcssfile = (filename) ->
  fileref = $("<link rel='stylesheet' type='text/css' href='#{filename}' />")
  $("head").append(fileref)

loadjsfile("vendor/javascripts/json2.js")
loadjsfile("vendor/javascripts/underscore.js")
loadjsfile("vendor/javascripts/backbone.js")
loadjsfile("vendor/javascripts/backbone.marionette.min.js")
loadjsfile("vendor/javascripts/backbone.relational.js")
loadjsfile("vendor/javascripts/jquery.hammer-full.js")

class ASCCView extends Backbone.Marionette.Layout
  template: _.template """
        <div id="header"></div>
        <div id="content"></div>
        <div id="footer"></div>
    """

  regions:
    header: "#header"
    content: "#content"
    footer: "#footer"

  initialize: ({@collection})->
    $(window).on('resize', @handleResize)

  remove: ->
    $(window).off('resize', @handleResize)
    super

  onRender: =>
    @contentView = new ascc.pTreeCollectionView( collection: @collection )
    @content.show( @contentView )

  handleResize: =>
    @contentView?.relayout()

class ASCCApplication extends Backbone.Marionette.Application


  onStart: =>
    @region = new Backbone.Marionette.Region( el: $('.ascc-appplication') )

    t1 = new ascc.Taxon(name: 'taxon1', minOKR: 5.5, maxOKR: 10.0)
    t2 = new ascc.Taxon(name: 'taxon2', minOKR: 7.0, maxOKR: 11.5)
    t3 = new ascc.Taxon(name: 'taxon3', minOKR: 8.5, maxOKR: 13.0)
    t4 = new ascc.Taxon(name: 'taxon4', minOKR: 10.0, maxOKR: 14.5)
    t5 = new ascc.Taxon(name: 'taxon5', minOKR: 11.5, maxOKR: 16.0)
    t6 = new ascc.Taxon(name: 'taxon6', minOKR: 13.0, maxOKR: 18.5)
    t7 = new ascc.Taxon(name: 'taxon7', minOKR: 14.5, maxOKR: 20.0)
    t8 = new ascc.Taxon(name: 'taxon8', minOKR: 16.0, maxOKR: 21.5)

    Root1 = new ascc.pTreeNode(label: 'Root1')
    Node1 = new ascc.pTreeNode( parent: Root1, label: 'Node1' )
    Node2 = new ascc.pTreeNode( parent: Root1, label: 'Node2' )
    Node5 = new ascc.pTreeNode( parent: Root1, label: 'Node5' )
    Node3 = new ascc.pTreeNode( parent: Root1, label: 'Node3' )
    Node4 = new ascc.pTreeNode( parent: Root1, label: 'Node4' )

    new ascc.pTreeNode( parent: Node1, label: t1.get( 'name' ) )
    new ascc.pTreeNode( parent: Node1, label: t2.get( 'name' ) )

    new ascc.pTreeNode( parent: Node2, label: t3.get( 'name' ) )
    new ascc.pTreeNode( parent: Node2, label: t4.get( 'name' ) )

    new ascc.pTreeNode( parent: Node3, label: t5.get( 'name' ) )
    new ascc.pTreeNode( parent: Node3, label: t6.get( 'name' ) )

    new ascc.pTreeNode( parent: Node4, label: t7.get( 'name' ) )
    new ascc.pTreeNode( parent: Node4, label: t8.get( 'name' ) )

    Root2 = new ascc.pTreeNode(label: 'Root2')
    Node1 = new ascc.pTreeNode( parent: Root2, label: 'Node3' )
    Node2 = new ascc.pTreeNode( parent: Root2, label: 'Node4' )

    new ascc.pTreeNode( parent: Node1, label: t5.get( 'name' ) )
    new ascc.pTreeNode( parent: Node1, label: t6.get( 'name' ) )

    new ascc.pTreeNode( parent: Node2, label: t7.get( 'name' ) )
    new ascc.pTreeNode( parent: Node2, label: t8.get( 'name' ) )

    @collection = new ascc.pTreeCollection([Root1, Root2])
    @view = new ASCCView( collection: @collection )
    @region.show( @view )

window.ascc = new ASCCApplication()

#loadcssfile("lib/ascc.css")
loadcssfile("lib/pTree.css")
loadjsfile("lib/pTreeNode.js")
loadjsfile("lib/taxon.js")
loadjsfile("lib/pTreeView.js")


